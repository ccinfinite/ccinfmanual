=====================
cc-infinite
=====================

**Computing Competences.
Innovative learning approach for non-IT students.**

.. figure:: img/ccinflogo.png
  :scale: 50

CC Infinite is aimed at anyone interested in learning programming. We do not want to limit this skill only to IT students or IT enthusiasts. We believe that every person, not necessarily related to the IT industry, can make their work with a computer more enjoyable and speed up their work by knowing the rules of programming. The project's main objective will be to increase the innovation and interdisciplinarity of higher education by developing a programming course for non-IT students and assessing its effectiveness in 4 European universities.

This manual was created as a result of the `CC Infinite project <https://ccinfinite.smcebi.edu.pl>`_ co-financed under the European Union program `Erasmus+ <https://www.erasmusplus.eu/>`_.

#################################
Interactive course in programming
#################################

.. toctree::
   :maxdepth: 1

   main/en_main_01_intro.rst
   main/en_main_02_variables.rst
   main/en_main_03_strings.rst
   main/en_main_04_io.rst
   main/en_main_05_collections.rst
   main/en_main_06_conditions.rst
   main/en_main_07_while_loop.rst
   main/en_main_08_for_loop.rst
   main/en_main_09_functions.rst
   main/en_main_10_functions.rst
   main/en_main_11_module.rst
   main/en_main_12_oop.rst

Teaching materials that we offer have been developed as part of an international consortium.

.. figure:: img/uni.png
    :scale: 80

.. figure:: img/ccdisclaimer.png
    :scale: 80
