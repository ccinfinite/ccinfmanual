.. _variables:

*********
Variables
*********

.. index:: literal, operator, primitives

We will start by discussing the simplest constructs of which practically all programming languages are built.
We can break down such *primitive* constructions into **literals**, which are values the programmer uses and **operators** to manipulate these literals (values).

Perhaps the simplest example is adding two numbers. We all know the result of addition: math:`2 + 2`.
To see the result of adding in the *ActiveCode* below, we need to wrap the action with the function ``print``.
Try the code below, then replace the numbers and the addition action (``+``) with something else you remember from your math lesson.

.. activecode:: en_main_0201
    :coach:
    :caption: Operations on numbers

    print(2 + 2)

.. index:: variable, assignment operator

We can also remember the result of the action. For this purpose, we create **variables** in programming languages. A variable is made very simply - we have to enter its name, then put the equal sign (**assignment operator**) and the value we want to assign to this variable, for example:

.. code:: Python

   variable = 2

In the window below, you can try creating variables. In this case, we immediately assign the result of adding the two numbers ``2 + 2`` to the variable ``addition_result``, and the difference ``2 - 2`` to the variable ``subtraction_result``.

.. activecode:: en_main_0202
   :coach:
   :caption: Create a variable

   addition_result = 2 + 2
   print ('Addition result:', addition_result)
   subtraction_result = 2 - 2
   print ('Result of subtraction:', subtraction_result)


.. topic:: Exercise

    In the above cell, add new lines of the program, in which you will calculate and print (using ``print``) the product and the quotient of any two numbers. You can still use ``2``, but maybe you want to try something a little more exciting...

As in math exercises, appropriate calculations can be grouped using parentheses. We do this for various reasons - to avoid a mistake or to specify the sequence of actions. If we would like to add the doubled value of the two numbers 3 and 4, write ``2 * 3 + 2 * 4`` or

.. activecode:: en_main_0203
    :coach:
    :caption: Parentheses

     result = 2 * (3 + 4)
     print (result)


.. index:: data type

In general, parentheses are used to group operations on any variable of any *type*, and it is not limited to operations with numbers. Brackets allow you to control the order in which the statements are executed. In most programming languages, we can use values (literals) with different properties. The value we provide and the visual representation will be defined by the **type** of the data.

.. topic:: Exercise

    While shopping, you found two pairs of shoes that you want to buy. The red ones cost 40 euros. The blue ones were 56 euros but with a 25% discount. For purchases over 85 euros, you will get an additional 20% discount!

    Which pair of shoes will cost more? Let's calculate the discounted price of the blue shoes, and then you can tick the appropriate answer below.

    .. activecode:: en_main_0204
       :coach:
       :caption: Brackets - Exercise

       blue_price = 56
       discount = (100 - 25) / 100
       new_price = blue_price * discount
       print (new_price)

    .. clickablearea:: en_main_ex0201
      :question: Which pair of shoes will cost more?
      :table:
      :correct: 1,2
      :incorrect: 1,1
      :feedback: Run above *ActiveCode* and check the result

      +------+-------+
      | red  | blue  |
      +------+-------+

    .. fillintheblank:: en_main_ex0202
       :casei:

       How much will both pairs cost?

       - :82: Excellent!
         :x: Incorrectly. Try to calculate the sum in the *ActiveCode* above.

    .. clickablearea:: en_main_ex0203
          :question: If you decide to buy both pairs, will you be able to get an additional discount?
          :table:
          :correct: 1,2
          :incorrect: 1,1
          :feedback: Go back to the *ActiveCode* and add both numbers together

          +------+------+
          | yes  | no   |
          +------+------+


Basic data types
======================

.. index:: int type, float type

Numeric types
~~~~~~~~~~~~~

We've seen several data types in the previous examples. Literals like ``2`` or ``2.0`` stand for the number 2 (a numeric literal with the value 2) but of two different types. The literal ``2`` has the value 2 and is assigned the integer type (``int``). The latter, ``2.0``, also has the value 2, but is of a floating-point type (``float``). We can treat both of them simply as the number 2, but the way they are stored in the computer's memory will differ. You can read more about the numerics in the other part of this project.

.. index:: bool type, True, False

Boolean type
~~~~~~~~~~~~

Particular data types differ in the allowed set values ​​and the allowed operations we can perform on them.
One of the most commonly used data types is the logical type (``bool``).
It can take the values ``True`` for logical truth or ``False`` for logical false. For example, we know that the number 2 is greater than the number 1. For this reason, writing: math:`2> 1`, we know that the expression is true. We can program such an operation.

.. code:: Python

    >>> 2 > 1
    True

Moreover, we can assign the result of such a comparison (``True``) to a variable.

.. activecode:: en_main_0205
    :coach:
    :caption: Boolean type

    print(2 > 1)
    truth = 2 > 1
    print(truth)

If we now return to the problem of buying shoes, instead of judging which pair is more expensive by looking at the bills, we can leave the conclusions to the interpreter.

.. activecode:: en_main_0206
    :coach:
    :caption: Shoes issues

    red_shoes = 40
    blue_shoes = 56 * 0.75
    print (red_shoes > blue_shoes)


.. index:: comparison operator

Basic operators are associated with the logical type, with which we can compare variables and obtain true or false as a result of such comparison. We have the operator ``>`` (greater than), which we already know, but also the operator ``<`` (less than). We can also check if a variable is "greater than or equal to" (``>=``) and "less than or equal to" (``<=``) another variable. We check for equality using the double equal sign ``==``, which is probably the source of one of the most common mistakes freshmen make. We test for inequalities using ``! =``. In the following *ActiveCode*, you can try out the comparison operators according to your ideas.

.. activecode:: en_main_0207
    :coach:
    :caption: Compare operations

    print(100 > 10)

We can perform the comparison operations on all objects. They all have the same priority, and the interpreter will analyze them from left to right.
In Python, we can also compare whether two objects are the same (more about objects in the chapter on :ref:`object-oriented programming <oop>`). The operators ``is`` and ``is not`` tell us just that. However, they will not be of much use in this course.

.. index:: type NoneType, None

Type 'NoneType'
~~~~~~~~~~~~~~~~

There is also a particular data type in Python called ``NoneType``. Within this type, we find only one value, ``None``. It is used to express the lack of a meaningful value, non-existence of data, or storing an *empty* content. It is also the value returned from the function when we omit the keyword ``return`` or when we do not give any value after this word. You can learn more about functions in :ref:`later <functions>` chapters of this manual.

.. index:: type function

``type`` function
~~~~~~~~~~~~~~~~~

To check what type a given object is, you can use the function ``type(obj)``.

.. code-block:: Python

     >>> type(3)
     <type 'int'>
     >>> type(7.5)
     <type 'float'>
     >>> type('a')
     <type 'str'>
