.. _oop:

*****************************
Object-oriented programming
*****************************

.. index:: class, instance, object

We reached the last chapter of this manual. We will discuss objects. In the case of the Python language, everything is an object, so this should probably be the first chapter, but it's too late for that.
We will not discuss objects, classes, and their instances very thoroughly here. We will only tell you how to quickly build a **class** and its **instance**, which represents this class.

    Everything is an object because it can be assigned to a variable or be an argument to a function.

To build an object that will represent some data and allow manipulation (change, extraction of relevant information...), we need to program the class. In Python, we use the following construction.

.. code-block:: Python

  class ClassName:
    BODY_CLASS

Classes can represent any objects invented by us. We can build classes that describe a duck, a pear, or a coffee table. These actual "objects" have specific physical attributes, such as color and weight, but we can also think about the features distinguishing objects. For duck and pear, it may be their species. For a table, it may be the type of material.

The class defines the state and behavior of objects. We can use it to describe what properties a given object has and what we can do with them.
Let's write a ``Pear`` class to illustrate the idea. We can describe such a pear by talking about its variety, color (which is probably related to the variety), weight, size (related to weight), taste...

.. code-block:: Python

    class Pear:
        "'
        The Pear object describes the properties of pears.
        "'

        def __init __ (self, variety, weight, taste):
            self.variety = variety
            self.weight = weight
            self.taste = taste

That is it! We have built a class that describes pears. There is nothing interesting we can do with this class yet. We've developed a class that defines an **object**. Now we can create one.
You may notice that right under the definition of ``class Pear:`` we put the string ``'''The Pear object describes the properties of pears'''``. As with functions, it is a string representing the class's documentation (docstring). Writing documentation is always a great idea.
Let's create an object based on this class.

.. activecode:: en_main_1201
    :caption: Pear

    class Pear:
        '''
        The class Pear describes the properties of pears.
        '''

        def __init__(self, variety, weight, taste):
            self.variety = variety
            self.weight = weight
            self.taste = taste

    pear = Pear('General Leclerc', 130, 'sour and sweet')
    print(pear.variety)

Now the variable ``pear`` stores an object of the class ``Pear``.
If I treat the class as a pattern that needs to be filled, then the object ``pear`` will be just a copy of such pattern of the class "Pear ", but *with specific values ​​(arguments)* given to the formal parameters of the class. In this case:

    ``variety`` <- 'General Leclerc'
    ``weight`` <- 130
    ``taste`` <- 'sour and sweet'

.. index:: instance, instance attribute, class attribute

We will call this a **class instance** or simply **an instance**. This instance has its **attributes**: ``variation``, ``weight``, and ``flavor``, which in this case are identical to the **class attributes**. We can refer to them as to ordinary variables (because they are also variables) using *dot notation*

.. code-block:: Python

    >>> pear.variety
    'General Leclerc'

By creating a new instance of the class with a call

.. code-block:: Python

    pear = Pear('General Leclerc', 130, 'sour and sweet')

we fill in the parameters that we declared in the definition of the function ``__init __(self, variant, weight, taste)``, an exceptional function, as you will see in a minute.

.. index:: init, __init__

Function ``__init__``
======================

This exceptional *special function* ``__init__``is used for passing arguments to the new instances of the class ``Pear``. To use arguments anywhere in the class (object) we have to assign them to **attributes**. We can break down attributes into class and instance attributes. Those belonging to instances begin in the class with the prefix ``self``.

.. index:: self

The variable ``self`` is an instance representation. Every function in the class body should begin with ``self`` [#self]_. In our example,

.. code-block:: Python

    pear = Pear('General Leclerc', 130, 'sour and sweet')

inside the body of the class, ``self`` magically turns into ``pear``. In fact, ``self`` does not have to be named ``self``. It can be called anything. Whatever it is called (although we recommend ``self`` though), it will be a **variable representing an instance inside the body of the class**. To refer to any instance attribute, we must precede this attribute in the class with the ``self`` variable and use the dot-reference.

.. index:: get, set, getter, setter

Getters and setters
====================

We should not refer directly to the instance's attributes (``variety, weight, flavor``), regardless of whether we want to read the value standing there or assign a new one. There are special functions called **getters** (for reading, their names start with the word ``get_``) and **setters** (for changing the value, ``set_``). They are built exactly like regular functions (with ``def``), and their first argument has to be ``self``.

.. activecode:: en_main_1202
    :caption: Pear - getters and setters

    class Pear:
        '''The class Pear describes the properties of pears.'''

        def __init__(self, variety, weight, taste):
            self.variety = variety
            self.weight = weight
            self.taste = taste

        def get_weight(self):
            return self.weight

        def get_variety(self):
            return self.variety

        def get_taste(self):
            return self.taste

        def set_weight(self, x):
            self.weight = x

        def set_variety(self, x):
            self.variety = x

        def set_taste(self, x):
            self.taste = x


    # instance
    pear = Pear('General Leclerc', 330, 'sour and sweet')

    # reference to the get_taste() method of the pear instance
    print("What is the flavor of a pear?")
    print("Pear is", pear.get_taste())

    # setting the flavor variable with set_taste(TASTE) for the pear instance
    pear.set_taste('extremely sweet')
    print("What is the flavor of a pear?")
    print("Pear is", pear.get_taste())

As you can see, the functions ``get_taste`` or ``set_taste`` are called like regular functions, but there is an instance name in front of it (here: ``pear``). Please note that although in the body of the class ``Pear`` the function ``get_taste`` (line 15) has one formal parameter ``self``, when calling this function (lines 42 or 48) **we do not provide an argument**. This is a typical notation (interface) for calling functions that belong to an instance. We call these functions **methods**.

    **Method** is an instance-related feature. With methods, we can influence the state of objects (instances).

In particular, it takes one argument less than its counterpart (function) in the class body because ``self`` receives the address of the instance (``pear``) from which we call the method. The remaining arguments appear in the call as in a traditional function.
Again - magically the instance name is substituted in the class under the variable ``self``.

.. index:: str, __str__

The class ``Square``
=====================

Now is the time for a more meaningful class example. We will build a relatively simple class to represent squares.
We will call it ``Square``. What a surprise! Naming classes with uppercase letters in the convention *CamelCase* is a good practice recommended by Python developers. The class will have minimal functionality: it will calculate the area and perimeter of the squares.

.. activecode:: en_main_1203
    :caption: Class Square
    :enabledownload:

    class Square:
        "" "Class Square" ""

        def __init__(self, a):
            self.set_side(a)

        def get_side(self):
            return self.side

        def set_side(self, var):
            if isinstance(var, (int, float)) and var > 0:
                self.side = var
            else:
                print('Side must be a number > 0')

        def area(self):
            return self.get_side() ** 2

        def perimeter(self):
            return 4 * self.get_side()

        def __str__(self):
            return "{} of area {} and perimeter {}".format(
                self.__class__.__name__, self.area(), self.perimeter())


    k1 = Square(10)
    print(k1)
    k2 = Square(3.1415)
    print(k2)

Experiment with the class above. Build other squares.
When you're done, read the paragraph below. It should clarify what and how such a class works. Finally, some exercises are waiting for you.

To describe a square, we only need one parameter - the side length. Knowing this parameter, we can calculate any quantity describing a square, such as its area or perimeter. The formal parameter ``a`` we provide when creating the instance. This time, to pass the value of the parameter ``a`` of the function ``__init__`` to the instance attribute named ``side`` (visible in the above program as ``self.side``), we use the helper function ``self.set_side(a)``. This function is written defensively and will not allow you to specify values ​​other than positive numbers of the type ``int`` or ``float``. However, if the user decides to enter some other value, he will see the information ``Side must be a number > 0``, and the variable ``side`` will not be created. With the correct input, such as in the two instance examples, the variable ``side`` will take the value given by the user, so we will be able to calculate the area and perimeter of the square.

Both utility functions ``area`` and ``perimeter`` take only one formal parameter ``self``, i.e., the corresponding instance. Thanks to this, the complete set of attributes and methods associated with the instance will be available to these functions through the construction ``self.ATTRIBUTE``. Among other things, we have the ``get_side`` method that returns the length of the side of a square. We must refer to it to be able to calculate the area and perimeter. We could use the ``self.side`` call directly. It will just be a different way to calculate the same characteristics without using the idea of ​​getter and setter. Both approaches have their pros and cons. The functions themselves are simple. When calculating the area, we raise the length of the side to the second power, and when calculating the perimeter, we add up the lengths of all four equal sides.

Next new concept is the special function ``__str__``. Do you still remember: ref:`problem of concatenating strings <string_concatenation>`? To convert any object to a string, we had to use the projection function ``str``. This function looks for in the objects of the special function ``__str__`` and calls it. So, when building such a function, we create a representation of our object in the type ``str``. What's more - the function ``print`` also displays what we program in the function ``__str__``. When creating a class, it is worth spending some time programming this special function. Thanks to this, we can clearly describe the object's instance that we will later work with.


.. topic:: Exercise

    Add a function that calculates the length of the diagonal to the ``Square`` class. Use formula :math:`d = a \sqrt{2}`.

.. topic:: Exercise

    Based on the ``Square`` class, build the ``Rectangle`` class. Remember,  that in general, have two sides, ``a`` and ``b`` of different lengths.


.. rubric:: Footnotes

.. [#self] we lie a bit here - not every function has to start with ``self``, but for now, let's leave it like that.
