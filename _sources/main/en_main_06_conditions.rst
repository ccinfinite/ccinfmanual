.. _branching:

********************
Branching
********************

.. index:: bool, boolean, True, False

We have already talked about the logical data type ``bool``. Within this type, we can distinguish only two values: ``True`` or ``False``. Many programming languages ​​take the number ``0`` for false and ``1`` (or any other number in general) for truth.
There are standard logical operators defined for this type.

.. index:: casting, explicit casting

The name of the logical type is also the name of the ``bool(obj)`` function, which allows you to change (cast) the variable ``obj`` to a logical type.

.. activecode:: en_main_0601
   :caption: bool, boolean type

   print("bool(1):", bool(1))
   print("bool(-3):", bool(-3))
   print("bool(0):", bool(0))
   print("bool(None):", bool(None))
   print("bool([1, 2, 3]):", bool([1, 2, 3]))
   print("bool([]):", bool([]))
   print("bool('Andy has a cat'):", bool('Andy has a cat'))
   print("bool(''):", bool(''))


We know from previous lessons that the basic logical operators are ``and`` (logical and), ``or`` (logical or), and ``not`` (logical negation). You will also find an overview of comparison operators (``<``, ``==``, etc.). Operations with them also result in literals of the type ``bool``.
This type is the basis for the construction of program branches.

**Branching** is a place in a computer program where we decide what the program should do next, based on a logical structure. In the simplest case, the program is divided into two parts.

The ``if`` statement
====================

.. index:: if, branch

In Python, the program branching statement is ``if``. Let us consider such a problem.

  If the value of the variable ``x`` is negative, change it to a positive number, otherwise do nothing.

We can *translate* such a mathematical task into a programming language.

  If variable ``x`` is less than 0, change its sign to the opposite, otherwise do nothing.

As you can see, we get two possibilities - either the number ``x`` is negative or it is not. We are dealing with branching. To write a program to do such a task, we need to use the ``if`` statement.

.. code-block:: Python

    if x <0:
        x = -x

.. topic:: Exercise

    This is not the only way to solve the problem of changing negative numbers to positive ones. How else can you change numbers from negative to positive? Try to find two other ways and write a program in Python.

..
  x *= -1
  x -= 2 * x
  x = abs(x)
  x = (x ** 2) ** 0.5

.. index:: indentation, code block

.. note:: **Indentation**

    To indicate that a given part of the code belongs to a given branch of the ``if`` statement, we must indent in Python, i.e., shift a part of the code a few spaces to the right. It is customary to use exactly four spaces to distinguish a given **block of code**. When learning to program, it may seem redundant and cumbersome. Fortunately, most word processors do it automatically for us. It is enough to start a given code block with the **colon** ``:``, for the editor to automatically shift the following line of code by four spaces. If you want to, you can use any number of spaces (and even tabs) to indent a block of code, but the best Python programmers use four.


The complete Python branching scheme looks like this:

.. code-block:: python

    if CONDITION:
        IF_INSTRUCTION_BLOCK
    elif CONDITION_1:
        BLOCK_1
    elif CONDITION_2:
        BLOCK_2
        ...
    else:
        BLOCK_ELSE

We see two new statements: ``elif`` (which we can read as *else if*) and ``else`` (*otherwise*). The obligatory construct is to use the keyword ``if`` and all other words are optional. They serve slightly more complicated branching. Python will check the above conditions one by one. If it encounters a **condition** that is true, it will no longer test the rest of them. Moreover, it will not even interpret the remaining blocks. The interpreter will only validate the syntax.

Let's look at an example. We will test whether the variable ``number`` points to an even number.

.. activecode:: en_main_0602
   :caption: even numbers

   number = 12
   if number % 2 == 0:
       e_or_ne = 'an even number'
   else:
       e_or_ne = 'not an even number'
   print(e_or_ne)

Of course, in the example above, we will see the message ``an even number``. Play with the code above and check for which numbers we get even numbers and for which not. The parity of a number means that it is divisible by two without a remainder, which means that the calculation of the remainder of the division using the operator ``%`` should return zero. We are testing this fact in the first branch ``if number% 2 == 0:``. We can answer "yes" or "no" to the parity question. There is no other option. In such a case, if the number turns out to be not even (the discussed condition would be equal to ``False``), we can use the option "in any other case" because it is the only other case. Therefore, we used the ``if-else`` construct. We face such a binary branching many times.

To show a slightly more complicated branch, we will write a program that will inform us how many days a given month has. To distinguish three possible answers (31, 30, and 28/29), we should use the ``if-elif-else`` construct. In addition, we can protect ourselves against a wrong answer if the user of our program provides a month number outside the $[1, 12]$ range. In this case, we will need two ``elif`` statements.

.. activecode:: en_main_0603
   :caption: Number of days in the month.

   days_31 = [1, 3, 5, 7, 8, 10, 12]
   days_30 = [4, 6, 9, 11]
   month = int(input('Enter the number of the month (from 1 to 12): '))
   if month == 2:
       print('Month nr {} has 28 or 29 days.'.format(month))
   elif month in days_31:
       print('Month nr {} has 31 days.'.format(month))
   elif month in days_30:
       print('Month nr {} has 30 days.'.format(month))
   else:
       print('A year has 12 months.')


.. topic:: Exercise

    Usually, when we ask for a month, we mean its name, not the order of the year. Try to rewrite the above program so that it asks the user for the month's name and, on its basis, returns information about the number of days. Probably it will be better to program the solution in some other environment, not here in *ActiveCode*. To make it easier download :download:`this script <en_main_ch06e03.py>`.
