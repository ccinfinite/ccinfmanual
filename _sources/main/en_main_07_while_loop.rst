.. _while_loop:

***************
``While`` loop
***************

Sometimes we have to execute a sequence of instructions over and over, perhaps even endlessly. In programming languages, we use loops in such cases. Python has two such statements that work on slightly different principles.

.. index:: while, loop

The universal Python construct that allows a block of statements to be executed multiple times is the while statement. Like branching, it is based on a logical condition.

.. code-block:: python

    while CONDITION:
        WHILE_BLOCK

As long as the condition is satisfied (equal to ``True``), the ``WHILE_BLOCK`` instruction block will be executed. As you can see, the statement block starts again after the colon. It's time for a simple example of a loop operation. To return the numbers from 1 to 5 raised to the second power, we can use the concept of a loop.

.. activecode:: en_main_0701
    :caption: squares of numbers from 1 to 5

    number = 1
    while number <= 5:
        square_number = number ** 2
        print ('{}^2 = {}'. format (number, square_number))
        number = number + 1

The last line (number 5) is essential. If we forget it, which happens unexpectedly often, the loop will continue forever.
Below, you will find a table that presents all the values of the variable ``number`` and the square and the test result ``number >= 5`` step by step. Note that the variable ``square_number`` will not be computed for ``number = 6``.

+------+--------------+-----------+
|number|square_number |number <= 5|
+======+==============+===========+
|  1   |      1       |    True   |
+------+--------------+-----------+
|  2   |      4       |    True   |
+------+--------------+-----------+
|  3   |      9       |    True   |
+------+--------------+-----------+
|  4   |     16       |    True   |
+------+--------------+-----------+
|  5   |     25       |    True   |
+------+--------------+-----------+
|  6   |              |   False   |
+------+--------------+-----------+

We can solve any programming task with two conditional constructs, ``if`` and ``while``. It will not always be easy, it may seem impossible at times, but we can usually do it. We will now try to find numbers divisible by 2 and 3, but those greater than 33 and smaller than 67.

.. activecode:: en_main_0702
    :caption: numbers divisible by A and B in the limit from start to stop

    start, stop = 33, 67
    A, B = 2, 3

    num = start
    while num <= stop:
        if (num % A == 0) and (num % B == 0):
            print('{} is divisible by {} and {}'.format(num, A, B))
        num = num + 1

As you can see above, we've put both conditions in parentheses in the conditional statement. In this way, we can *group* calculations or instructions that should be executed separately, and we are not sure if a notation without parentheses will work. In this case, removing the parentheses will not change anything, as the logical product ``and`` has one of the `lowest priorities among the operators <https://docs.python.org/3/reference/expressions.html#operator-precedence>`_. Regardless of the precedence of the operation, parentheses often significantly increase the readability of the code, and we encourage you to use them (wisely!).

.. topic:: Exercise

    Modify the above program to calculate the **sum** of all numbers divisible by 2 and 3, greater than 33 and smaller than 67.

.. index:: while - else

As for the branches, we can use the ``else`` statement in the ``while`` loop.

.. code-block:: python

    while CONDITION:
        WHILE_BLOCK
    else:
        ELSE_BLOCK

We can understand it precisely like the ``if-else`` construction. When 'CONDITION' is not met, the statement block under 'else' will execute. We have two such options:

* the ``while`` loop will run to the end, and the ``else`` block will be called after it

    .. code-block:: python

        i = 1
        while i < 8:
            print(2 ** i)
            i += 1
        else:
            print('end of loop')

* the ``while`` loop will not execute at all (the condition will not be met),
   the ``else`` block will be executed

    .. code-block:: python

        i = 10
        while i < 8:
            print(2 ** i)
           i += 1
        else:
            print('while loop will not execute at all')


.. index:: break, continue

The phrases ``break`` and ``continue``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If we expect the loop to break at some point, we can use the keyword ``break``.

.. activecode:: en_main_0703
    :caption: break

    j = 10
    while True:
        if j > 23:
            print('stop: j = {} is greater than 23'.format(j))
            break
        j += 3

.. note:: **while True**

    Such a construction is used when we want to build an endless loop. Since the condition ``True`` will always be true, we have to use another way to stop the loop. We can use the ``break`` statement as in the example above. We can also call :ref:`subroutine <function>`, which will control the program. We will talk about subroutines soon.


The statement ``continue`` has precisely the opposite behavior. When it is encountered, the instructions in the loop block will stop executing, and the loop will rotate again, regardless of the program's state.

.. activecode:: en_main_0704
    :caption: continue (the number -3 will not print)

    j = -5
    while j <= 5:
        j += 1
        if j == -3:
            print('we omit {}'.format(j))
            continue
        print('cube {} to {}'.format(j, j**3))


.. topic:: Exercise

    Sometimes, when we run a computer program, it will ask us various questions, usually needed to configure it correctly. Sometimes the answer is limited to deciding "yes" or "no", so if we write "ok", the program will not know what we mean - we will have to enter precisely either the word "yes" or the word "no", and if we give another, then the program will return to the same question and keep asking until we enter one of these words. Using an infinite loop and the ``break`` statement, write such a program. Below we present the skeleton of this program. In place of comments, enter your code.

.. activecode:: en_main_0705
    :caption: exercise
    :enabledownload:

    while True:
        ans = input('Enter yes or no: ')
        # if yes: print 'Agreed!' and end the loop (program)
        # if not: print 'No consent!' and end the loop (program)
        # if different: ask again to enter yes or no
        break
