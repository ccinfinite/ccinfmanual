.. _strings:

*******
Strings
*******

.. index:: string, string, str, index

We create a string of characters using a pair of apostrophes ``"..."`` or quotation marks ``'...'`` between which you can enter any characters you find on the keyboard, and even those you will not find on it.
In Python, a type that represents a string is called ``str``.

We can look at the ``'Spam and Eggs'`` string as if it were
a set of characters, where the first character is ``S``, the second is ``p``, the third is ``a``, and so on. So we can number characters in such a string. In Python, it is customary to start numbering from ``0``. In such a case, the first character in the string will be represented by the number ``0``, and the last character by the number ``string_length - 1``. For our string, the letter ``S`` will correspond to the number ``0``, the letter ``p`` to the number ``1``, the letter ``e`` to the number ``2``, etc. These numbers (0, 1, 2 ...) are called **indices**.
Indices are used to point to a specific place in a string of characters. When pointing to a particular place, e.g., fifth, we refer to a character with index 5. The fifth index will, in this case, be the sixth character in the string (in our case, the letter ``a``).
Such a reference is made with square brackets:

.. code-block:: python

    string[index]

Based on the ``'Spam and Eggs'`` string, we will refer to the indices ``2`` and ``5``, that is, to the third and sixth place in the chain, respectively. Let's see how it works in the *ActiveCode*

.. activecode:: en_main_0301
    :caption: Sequence indexing

    text = 'Spam and Eggs'
    print('(a) text[2] =', text[2])
    print('(a) text[5] =', text[5])


.. index:: len, sequence length

The last index in the variable ``text`` is the number 12 and is equal to
``string_length - 1``. To calculate the length of the sequence, we can use the function ``len('text')``, which means that the last available index of the sequence ``seq`` is given by the formula ``len(seq) - 1`` and with this expression, we can refer to the last element of the sequence.

.. code-block:: Python

     seq[len(seq) - 1]

The second last item will be ``seq[len (seq) - 2]`` etc.
Printing (and computing) the length of the sequence each time is not
particularly advantageous, and Python allows us to reference *from the sequence end* simply by using negative indices.

.. activecode:: en_main_0302
    :caption: indexing the sequence with negative indexes

    text = 'Spam and eggs'
    print('(last s) text[12] ->', text[12])
    print('(last s) text[len(text) - 1] ->', text[len(text) - 1])
    print('(last s) text[-1] ->', text[-1])
    print('(last g) text[-2] ->', text[-2])
    print('(S) text[-13] ->', text [-13])


The table below summarizes the possibilities for referencing individual places in the sequence ``s = "Python"``.

=============== ========== ========== ========== ========== ==========
P               y          t          h          o          n
=============== ========== ========== ========== ========== ==========
0               1          2          3          4          5
len(s) - len(s) len(s) - 5 len(s) - 4 len(s) - 3 len(s) - 2 len(s) - 1
-len(s)         -5         -4         -3         -2         -1
=============== ========== ========== ========== ========== ==========

Operations on strings
~~~~~~~~~~~~~~~~~~~~~

.. index:: concatenation

We can perform various operations on simple variables such as numbers. We can add them, subtract them ... we talked about it :ref:`in the previous chapter <variables>`. It is no different with chains. We can add such strings together using the operator ``+``.

.. code-block:: python

     >>> 'Spam' + 'eggs'
     'Spameggs'

We can also multiply them by a number (although only of the ``int`` type).

.. code-block:: python

     >>> 'Spam' * 3
     'SpamSpamSpam'

Try it yourself

.. activecode:: en_main_0303
     :caption: simple string operations

     print('Spam' + 'eggs')
     print('Spam' * 3)

Everything works the same when we first capture the string into a variable.

.. .. showeval:: showEval_concat_multi
..    :trace_mode: true
..
..    tekst = 'Mielonka'
..    inny_tekst = 'jajka'
..    ~~~~
..    {{'Mielonka' + 'jajka'}}{{tekst + inny_tekst}}
..    {{'Mielonka' * 3}}{{tekst * 3}}


.. activecode:: en_main_0304
     :caption: simple operations on string variables

     text = 'Spam'
     other_text = 'eggs'
     print(text + other_text)
     print(text * 3)


Take a look at the shortcode below.

.. code-block:: python
    :linenos:

    text = 'Buy an egg'
    result = (text[8] + text[4]) * 2


.. fillintheblank:: fitb-en_main_03ex1

   What value will you assign to the variable 'result'?

   - :gaga: Correct.
     :ga: See multiplication by 2 at the end.
     :e e e: In Python, indexing starts from 0.
     :x: Unfortunately not. Take a look at the numbers in square brackets - what letters do these numbers refer to? Put those letters together, and don't forget to multiply by 2. Maybe you didn't include the apostrophes (or quotation marks) in your answer?


We can compare the strings with each other. If we want to ask if two strings are the same (consist of the same characters), we can ask.

.. activecode:: en_main_0305
      :caption: strings comparison

      text = 'Spam'
      same_text = 'Spam'
      other_text = 'eggs'
      print('Is Spam a Spam?', text == same_text)
      print('Is Spam not eggs?', text != other_text)

It is a bit more complicated for other comparison operations. In this case, Python compares the strings element by element. For a pair of characters, the interpreter compares the values of the characters from the given character table (ASCII, Unicode ...). If the values differ in the next pair of characters, the string with the greater value in the character array is found as the greater one. You can learn more about string comparisons in the section on string manipulation.


.. _sequence_slicing:

Sequence slicing
~~~~~~~~~~~~~~~~

We can also use indices to refer to a larger part of a sequence. We can use them in *sequence slicing*. A general method of sequence slicing looks like this:

.. code-block:: python

    seq[start:stop:step]

As you can see, we generally have to give three numbers:

* ``start`` - the index from which we start the slicing
* ``stop`` - index at which the slicing ends, the value corresponding to this number is omitted
* ``step`` - the size of the increment

Let's take a look at a simple example. Please note that for all the arguments, we can substitute both positive and negative numbers and zero.

.. activecode:: en_main_0306
    :caption: sequence slicing

    text = 'Spam and eggs'
    print('(egg) from 9 to 12 every 1:', text[9:12:1])
    print('(mne) from 3 to 12 every 3:', text[3:12:3])
    print('(Sa n gs) from start to finish every 2', text[0:len(text):2])
    print('(sgge dna) from the end to 4 with a negative step', text [-1:4:-1])

A particular type of step is the one that equals ``1``. If ``step = 1`` it means we mean each subsequent index in the desired range. It is a default value of the increment, and we can omit it.

.. code-block:: python

  >>> text[2:5] == text [2:5:1]
  True

The specific indices are ``0`` and ``-1`` (``len(seq) - 1``). They mean
the start and end of the sequence. These places are obvious to the interpreter, and we can skip them.

.. activecode:: en_main_0307
    :caption: Indices 0 and -1

    text = 'Spam and eggs'
    print('from start to 5 every 1:', text[:5])
    print('from 8 to the end of every 2:', text[8::2])
    print('a copy of sequence - beginning to end every 1', text[:])
    print('reversed sequence', text[::-1])

Similar references can be made to any type of sequence.

We come back to the previous task. This time we want to use the variable ``text`` to assign ``'nana'`` to the variable ``result``, but this time using the above string cutting structure.

.. code-block:: python
    :linenos:

    text = 'Buy an egg'
    result = text[start:stop:-1] * 2

.. fillintheblank:: fitb-pl_main_03ex2

    What will the code look like so that the result variable will contain the string ``'nana'``?

    result = tekst[|blank|:|blank|:-1] * 2

    - :5: Poprawnie.
      :6: Język Python numeruje pozycje w łańcuchu od zera.
      :x: Niestety nie.
    - :3: Poprawnie.
      :4: Nie do końca. Pamiętaj, że druga wartość nie jest uwzględniana w wycinaniu. Ponadto język Python numeruje pozycje w łańcuchu od zera.
      :x: Niestety nie.


.. index:: operator in, operator not in, in, not in

Membership operators
--------------------

There are special membership operators for sequences. The operator ``in`` checks if an element is present in the sequence. The sister operator ``not in`` checks if a piece is missing.

.. activecode:: en_main_0308
     :caption: Membership operators: in, not in

     text = 'Spam and eggs'
     print('Is S in the text?', 'S' in text)
     print('Is z not in the text?', 'z' not in text)

As you can see, the result of these operations is a logical value, either ``True`` or ``False``.

.. _strings_concatenation:

.. index:: concatenation, str function

Concatenation of strings and the ``str`` function
---------------------------------------------------

In one of the examples above, we used the ``+`` operator to add two characters together. We can concatenate strings of any length and, as a result, create a new longer string.

.. activecode:: en_main_0309
     :caption: Concatenation

     print('Spam' + 'and' + 'eggs')
     variable = 'Spam and eggs.' + ' ' + 'Eggs and spam.'
     print(variable)

If we would like to generate a sequence of characters in such a way by attaching the number (``int`` or ``float``) to a string, then simply by adding such quantities together, we will see that we get an error.

.. index:: casting, function str

.. code-block:: python

    >>> number_of_cats = 2
    >>> 'Ala has ' + number_of_cats + ' cats.'
    TypeError: cannot concatenate 'str' and 'int' objects

In a way, we might expect this because the operator ``+`` denotes something different for numbers and strings. To *dynamically* create a string using the operator ``+`` and the numeric variable ``number_of_cats`` and get ``'Ala has 2 cats.' '', we will have to somehow convert the number ``2`` to the type ``str``.
It is known as **casting** of the type ``int`` to the type ``str``. To perform it, we use the function with the name of the requested type. Here it will be ``str`` as we need a string representation of the number ``2``. It is enough to *wrap* the variable we are interested in with the function ``str(number_of_cats)``. This function will return the representation of a given variable as the string type.

.. activecode:: en_main_0310
    :caption: Concatenation with casting

    number_of_cats = 2
    print ('Ala ma ' + str(number_of_cats) + ' cats.')

Most of the built-in types have this representation, and we can append any other literals to strings. Below is an example with a list, but feel free to try different types too!

.. activecode:: en_main_0311
    :caption: A function that projects str

    fib_list = [1, 1, 2, 3, 5, 8, 13, 21]
    fib_info = '8 words of the Fibonacci sequence are ' + str(fib_list)
    print(fib_info)

The ``format`` method
=====================
Strings have a built-in method called ``format``, which allows the strings to be created rather dynamically without concatenation or a casting function, which sometimes can be tricky. The ``format`` method is an easier way to do just that.

If we have three variables, ``a, b, c`` and want to insert them into the string using the ``format`` function, we write:

.. code-block:: python

    '{}, {}, {}'.format(a, b, c)

We can also use numbers to specify the order of the arguments. It allows the order of the variables in the string to be different from the order of the function's arguments.

.. code-block:: python

    '{0}, {1}, {2}'.format(a, b, c)
    '{2}, {1}, {0}'.format(a, b, c)

You can also use variables several times.

.. code-block:: python

    {0}{1}{0} '.format('abra', 'cad')

We can also use argument names.

.. code-block:: python

    'Coordinates: {lat}, {long}'.format(lat='37.24N ', long='- 115.81W')

It's a good idea to use the 'format' function to create strings, as it gives you a lot of control over the result. This lesson only touches on the topic of strings and their formatting. We recommend the `PyFormat document <https://pyformat.info>`_ or reading about string manipulation in this project's sister manual if you want to learn more about formatting strings.

.. activecode:: en_main_0312
    :caption: str.format()

    a, b, c = 'one', 'two', 'three'
    print('{}, {}, {}'.format(a, b, c))
    print('{0}, {1}, {2}'.format(a, b, c))
    print('{2}, {1}, {0}'.format(a, b, c))
    print('{0}{1}{0}'.format('abra', 'cad'))
    print('Coordinates: {latitude}, {longitude}'.format(latitude='37.24N ', longitude='-115.81W'))

.. topic:: Exercise

    TBA
