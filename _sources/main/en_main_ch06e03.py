# Try rewriting the above program to prompt the user for a name
# of the month and on its basis return information about the number of days.

days_31 = [1, 3, 5, 7, 8, 10, 12]
days_30 = [4, 6, 9, 11]
month = int(input('Enter the number of the month (from 1 to 12): '))
if month == 2:
   print('Month nr {} has 28 or 29 days.'.format(month))
elif month in days_31:
   print('Month nr {} has 31 days.'.format(month))
elif month in days_30:
   print('Month nr {} has 30 days.'.format(month))
else:
   print('A year has 12 months.')
