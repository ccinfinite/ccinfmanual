.. _io:

******************************
Input and output
******************************

Input and output operations are responsible for reading and writing data, respectively.
The basic operations that will interest us are related to simple communication with the user. All in all, we mean displaying information on the screen and reading the characters entered from the keyboard. The functions ``print`` and ``input`` are used for this, respectively.

.. index:: print, simple print formatting

The ``print`` function
~~~~~~~~~~~~~~~~~~~~~~

To write something on the screen, we have to use the ``print`` function.  Just after its name, we should put the object you want to see in parentheses. It can be some value given explicitly or a variable.

.. code-block:: python

    >>> print (13)
    13
    >>> z = 13
    >>> print (z)
    13

In this simple formatting, we only display the content of the variables. We can also show a series of variables/values. It is enough to separate them with the comma, one by one.

.. activecode:: en_main_0401
    :caption: print - simple formatting

    name = 'Ala'
    age = 13
    print(name, "is", age, "years old")

.. topic:: Exercise

    Using the example above, try adding a few more variables and display that ``'Ala is 13 years old, lives in the city of Katowice, and her favorite sport is basketball.'`` with the ``print`` function.

Optional arguments to the function ``print``
-----------------------------------------------

.. index:: print argument sep

The default character which separates the elements of the list of values ​​printed by the function ``print`` is a space. You can see this in the example above. However, we can change this sign freely. The argument 'sep' is used for this. To change the default space for a '+' sign, type

.. code-block:: python

    >>> print('Ala', 'is', '13', 'years', 'old', sep='+')
    'Ala+is+13+years+old.'

To replace ``+`` with ``---`` it is enough to replace the given separator with the new one you want.

.. showeval:: showEval_print_sep
   :trace_mode: false

   print('Ala', 'is', '13', 'years old.', sep='+')
   ~~~~
   print('Ala', 'is', '13', 'years old.', sep='{{+}}{{---}}')


Simple, isn't it? Try for yourself. Below you have *ActiveCode*. Replace ``+`` with another separator, like: ``'@('_')@'``, you will see how it works.

.. activecode:: en_main_0402
   :caption: print - sep

   print('Ala', 'is', '13', 'years old.', sep='+')


.. index:: print argument end

As with the separator, we can independently determine how ``print`` will end the display of the argument list. For that, we use the ``end`` argument. By default, ``print`` breaks the line and goes to a new one, which means that ``end`` is set to the command *break line*, which we write ``end='\n'``. The characters ``\n`` are special characters that mean a line break. There are more of them. If you are interested, please refer to the part of this manual dealing with strings manipulation.

When we give four variables in four calls to the ``print`` function, we will see them in four separate lines.

.. code-block:: python

   print('Ala')
   print('is')
   print('13 ')
   print('years old.')
   Ala
   is
   13
   years old.

On the other hand, if we set the argument ``end=''`` (or anything else, similar to ``sep``), we will see that print will not go to a new line after displaying the string, but instead of breaking it, it will put the given character there. Note that the last ``print`` in the code below should break the line.

.. activecode:: pl_main_0403
    :caption: print - end

    print('Ala', end=' ')
    print('is', end='... ')
    print('13', end="!!! ")
    print('years old.', end='\n')

.. index:: print argument flush, print argument file

Finally, we'll mention two other optional arguments: ``file`` and ``flush``. They are used when writing the contents of variables to a file. The handle of the file we would like to append is passed to the variable ``file``. The second argument, ``flush`` is a boolean variable. It allows forcing of printing the content immediately (when we set it to ``True``) or caching it (``False`` ). Let's not worry about it for now, because we won't need it for now.
We will also talk about files and handles on another occasion.


The ``input`` statement
~~~~~~~~~~~~~~~~~~~~~~~~~

Sometimes we want to introduce a bit of interaction into our programs, ask the user something and expect an answer (yes/no). To pass some value to our program dynamically, regardless of a number or some text, we use the ``input(message)`` function. This ``message`` is a string that the user will see before entering a value. Let's see how it works.

.. activecode:: en_main_0404
    :caption: input

    name = input("What's your name? ")
    age = input('How old are you? ')
    print(name, 'is', age, 'years old.')


The data entered by the user is not interpreted, and the type of the value stored in the variable is always a string (``str``).

Sometimes, however, the values given by the user are numeric, and we would like to capture them into variables as numbers to calculate something from them. To enable this, it is enough to cast explicitly what the function ``input`` takes to the type of interest. To convert the retrieved data to an integer, we need to use the int function. For floating-point numbers, we will use the float function.

.. activecode:: en_main_0405
    :caption: input, int and float

    cats = int(input('How many cats do you have?: '))
    print('You are at home', 4 * cats, 'furry legs!')

    height = float(input('How tall are you (in meters)? '))
    print('You have {} centimeters!'.format(height * 100))


.. topic:: Exercise

    Using the previous exercise and the example above, write a short program that asks the user for some additional information. In addition to the name and age, ask about his favorite sport and the city he lives in. Finally, use ``print`` to display this information as clearly as possible. You can use the previous examples and the *ActiveCode* above to program your solution.
