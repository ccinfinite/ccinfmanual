.. _complex_data_types:

*****************************************
Some of the most popular complex types
*****************************************

Sometimes it is convenient to group objects together, for example, by creating a list of students' names in class, a list of Pearl Jam's favorite songs, or creating a catalog of books. We can also keep grades for specific items in similar collections. Building separate variables for your five favorite Pearl Jam songs is doable but not very efficient.

.. code-block:: python

   pj_no1 = 'Black'
   pj_no2 = 'Crazy Mary'
   pj_no3 = 'Rearviewmirror'
   pj_no4 = 'Better Man'
   pj_no5 = 'Sirens'

It would be wiser to make a list of these songs and put your favorite song *on the actual list* in the first position, your second favorite song in the second position, etc...

.. index:: list, lista, obiekt modyfikowalny

Lists
~~~~~

A **list** (Python's ``list`` type) may come in handy.
The list is the most general type of sequence. Just like :ref:`strings <strings>` objects of this type are ordered sequences. The main advantage of lists is the simplicity of processing their elements. Operations are similar to those on strings as we can slice them, refer to indices or check their length. We can create a variable of type ``list`` in several ways. The most basic will be to use square brackets, which is an **interface** dedicated to lists.

.. code-block:: python

    L1 = [10, 20, 30]
    L2 = []

The first object named ``L1`` will be a list with three numeric elements ``10, 20, 30`` and the second ``L2`` will be an empty list (a list with no elements). Therefore, the former length is three, and the latter is 0 - you can check it with the same function that you used to check the size of the string ``len(list)``. Going back to the tracklist example, it would look like this.

.. activecode:: en_main_0501
    :caption: Pearl Jam's top five

    pj = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    print(pj)


As you can see, the first song on the list is the song that was previously assigned to the variable denoting number one hit. To refer to this name, we will use the same reference by index as for strings.

.. code-block:: python

    pj[0]

It will return 'Black'.

.. index:: concatenating lists

Lists can be concatenated using the ``+`` operator.
We can create a new list by combining these two lists with the addition operator ``L1 + L2``. The new list will consist of all the elements of both lists. ``L1`` elements will be at the beginning, and the ``L2`` elements will be found at the end.

.. activecode:: en_main_0502
    :caption: Pearl Jam's Top Ten

    pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    pj2 = ['Alive', 'Yellow Ledbetter', 'Jeremy', 'Even Flow', 'Once']
    pj = pj1 + pj2
    print(pj)


We can modify lists. The most straightforward modification is to change a specific item using the reference ``list[index]`` and use the assignment operator. We can, for example, decide that starting from today, the 5th best Pearl Jam song for us will not be *Sirens*, but *Hail, Hail*. In this case, it is enough to refer to the fifth place in the list (remembering that we start numbering from zero) and assign a new song.

.. showeval:: showEval_list_mod
   :trace_mode: true

   pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
   ~~~~
   {{'Sirens' --> 'Hail, Hail'}}{{pj1[4] = 'Hail, Hail'}}
   {{pj1[4] = 'Hail, Hail'}}{{}}
   pj1 --> ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', {{'Sirens'}}{{'Hail, Hail'}}]

Try it yourself. You can also change *Hail, Hail* to a completely different title.

.. activecode:: pl_main_0503
    :caption: modification by reference to an index

    pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    print('Oryginalna lista', pj1, sep='\n')
    pj1[4] = 'Hail, Hail'
    print('Zmodyfikowana lista', pj1, sep='\n')


As with strings, we can use :ref:`cut sequence <sequence_slicing>`, via known reference.

.. code-block:: python

    list[start_index:end_index:step]


Modifications and other operations specific to lists
-----------------------------------------------------

.. index:: OOP, object, method, attribute

Python is an object-oriented language. We will tell you more about it at the end of this manual. For now, it is enough to know that an object contains both data and methods of manipulating this data. You can understand data as just some values ​​that you assign to variables. For example, ``L = [1, 2, 3]`` would be a list named ``L``with three elements ``1, 2, 3``.

To operate on such a list (as well as on other objects) using methods embedded in them, we use the so-called **reference by a dot**. The syntax for such a reference is.

.. code-block:: python

    # method reference
    object.method(arguments)
    # reference to an attribute (field)
    object.attribute

.. index:: append

Lists have several such methods, and we will discuss them at the end of this section. For now, we will focus on one of the most used - the ``append`` method for extending the list with another element. If we already have the list mentioned above of Pearl Jam's five favorite songs ``pj1``, but we want to add two more pieces to it, then we can write

.. activecode:: en_main_0504
    :caption: modification by an index reference

    pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']
    print('Original list', pj1, sep='\n')
    pj1.append('Hail, Hail')
    print('Extended list', pj1, sep='\n')
    pj1.append('You Are')
    print('List of 7 elements', pj1, sep='\n')

We hope you understand this because now it's time for an exercise ...

.. topic:: Exercise

    Using the ``append`` method, extend the original playlist ``pj1 = ['Black', 'Crazy Mary', 'Rearviewmirror', 'Better Man', 'Sirens']`` with another 5 songs: 'Alive', 'Yellow Ledbetter', 'Jeremy', 'Even Flow', 'Once' (or whatever else you would choose). You can use the *ActiveCode* above or program the answer in the Jupyter notebook.


There are many methods to manipulate lists.

* ``L.append(obj)``: adds an item to the end of the ``L`` list
* ``L.extend(new_list)``: Extends the list by including all elements of the specified list ``new_list``
* ``L.insert(idx, obj)``: inserts the element ``obj`` at the given position the ``idx`` list
* ``L.remove(obj)``: removes the first encountered element ``obj`` from the list; if there is no such item in the list, an error is reported
* ``L.pop(idx)``: removes an item from the given ``idx`` item in the list and returns it as a result; if no index is given, ``L.pop()`` removes and returns the last item in the list
* ``L.index(obj)``: Returns the index of the first item in the list
* ``L.count(obj)``: Returns the number of occurrences of ``obj`` in the list
* ``L.sort()``: sorts the items in the list itself; this is an operation irreversibly modifying the list ``L``
* ``L.reverse()``: Reverses the order of the items in the list itself

We'll try a few of these: We'll make a list L and append the squares of numbers from 1 to 10. Then we will remove and add some numbers by various methods.

.. activecode:: en_main_0505
    :caption: some operations on lists

    L = [10, 20, 30]
    print('start: L =', L)

    L.insert(0, 1001)
    L.append(100)
    print('append + insert: L =', L)

    L.pop(2)
    del L[2]
    print('pop + del: L =', L)

    L.reverse()
    print('reverse: L =', L)


.. index:: tuple

Tuples
~~~~~~

Tuples are lists that we cannot modify. We create them
with parentheses ``()``.

.. code-block:: python

    three_elements_tuple = (1, 2, 3)
    single_element_tuple = ('Ala',)
    empty_tuple = ()

In the example above, ``three_elements_tuple`` is a tuple with three elements (``len(three-element tuple)`` will return the number 3),
A ``single_element_tuple`` is a tuple with one element
and ``empty_tuple`` is an empty tuple.
Note the mandatory comma when creating single-element tuples ``('Ala',)``. Tuples can be concatenated using the operator ``+``.

.. activecode:: en_main_0506
    :caption: tuple concatenation

    k1 = (1, 2, 3)
    k2 = ('Ala', 'has', 'a cat')
    k3 = k1 + k2
    print(k3)

Tuples as immutable objects have the methods limited to ``count`` and ``index``, which work precisely like the appropriate list methods.

.. activecode:: en_main_0507
    :caption: two tuple methods

    k1 = (1, 1, 3, 2, 3, 3)
    print('k1.count(3):', k1.count(3))
    print('k1.index(3):', k1.index(3))

A tuple element cannot be removed with the ``del`` command, as we cannot modify them.


.. index:: dict

Dictionaries
~~~~~~~~~~~~

Apart from the above two **sequential types**, another helpful and frequently utilized complex type is **dictionaries**. What distinguishes them from lists, tuples, and strings is their lack of ordering and the fact that each element in a dictionary consists of two parts - a key and a value. The key is a quantity that identifies the value in the dictionary. We build dictionaries using curly braces.

.. code-block:: python

  dictionary = {key1: value1, key2: value2, key3: value3}


If, for example, we would like to create a database of information about our friends' birthdays, we can use a dictionary. We will use the keys to mark friends' names, and the value will be the date of birth.


.. code-block:: python

     birthday = {'Amanda': 'January 12', 'Barbara': 'February 17', 'Carol': 'August 2'}


When we want to remember Amanda's date of birth, we need to refer to the element whose ``key = 'Amanda'``, using square brackets, similar to lists, tuples, and strings.


.. activecode:: en_main_0508
    :caption: Birthday dictionary

    birthday = {'Amanda': 'January 12', 'Barbara': 'February 17', 'Carol': 'August 2'}
    print(birthday['Amanda'])


In this way, we can find out on which day our friends have their birthday if we accidentally forget it.

.. topic:: Exercise

     Try the above *ActiveCode* by replacing the name with some other first name from the list. You can also add your own ``name: birthdate`` pairs.

In a relatively simple way, we can modify already existing elements of the dictionary. Just refer to a dictionary item with a key and assign a new value.

.. activecode:: en_main_0509
    :caption: Modification of a dictionary item

    birthday = {'Amanda': 'January 12', 'Barbara': 'February 17', 'Carol': 'August 2'}
    print(birthday)
    birthday['Amanda'] = 'August 30'
    print(birthday)

Similar to the previously discussed complex types, dictionaries have many built-in methods that allow you to manipulate them. They are all available using the dot reference. We'll cover some of them in a moment. Now we will focus on the possibility of enlarging the dictionary with new elements. The simplest option is to enter a new key and value as if you want to modify an existing pair.

.. activecode:: en_main_0510
    :caption: Add a dictionary item

    birthday = {'Amanda': 'August 30', 'Barbara': 'February 17', 'Carol': 'August 2'}
    birthday['Mike'] = 'November 20'
    print(birthday)

.. topic:: Exercise

    In the *ActiveCode* above, add the name and day of birth of someone you know. It could also be someone as famous as Albert Einstein.

.. index:: del

To easily remove a ``key: value`` pair from the dictionary, we can use the ``del`` command to remove variables from the namespace.

.. activecode:: en_main_0511
    :caption: Delete a dictionary item

    birthday = {'Amanda': 'August 30', 'Barbara': 'February 17', 'Carol': 'August 2'}
    del birthday['Carol']
    print(birthday)

In addition to these basic operations using the square brackets interface, dictionaries can be modified using methods built into them. Below you will find a list of methods with a brief overview. We assume that ``d`` is a dictionary.


* ``d.clear()`` Removes all items from the dictionary ``d``
* ``d.copy()`` Returns a copy of the dictionary ``d``
* ``d.fromkeys(klucze, default_value)`` Returns the dictionary ``d`` with the specified keys ``keys`` and the value set to the optional variable ``default_value``
* ``d.get(klucz, default_value)`` Returns the value of the given key ``key``, if there is no key returns the value of the variable ``default_value``
* ``d.items()`` Returns a list containing a tuple for each ``key: value`` pair
* ``d.keys()`` Returns a list containing the keys of a dictionary
* ``d.pop(klucz)`` Deletes an element with the specified key ``key``
* ``d.popitem()`` Deletes the last inserted key-value pair
* ``d.setdefault(key, value)`` Returns the value of the specified ``key``. If the ``key`` does not exist, it creates a ``key: value`` pair in the ``d`` dictionary
* ``d.update(d)`` Updates dictionary ``s`` with elements of dictionary ``d`` overwriting existing pairs
* ``d.values()`` Returns a list of all the values ​​in the dictionary

The learned operations allow for relatively free manipulation of dictionaries. For example, we can use two ways to modify the birthdate dictionary.

.. activecode:: en_main_0512
    :caption: Modification of a dictionary element

    birthday = {'Amanda': 'August 30', 'Barbara': 'February 17', 'Carol': 'August 2'}
    print(birthday)
    # 1
    birthday['Carol'] = 'August 3'
    print(birthday)
    # 2
    birthday.update({'Carol': 'August 4'})
    print(birthday)
