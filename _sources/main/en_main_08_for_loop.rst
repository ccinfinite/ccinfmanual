.. _for_loop:

*************
``for`` loop
*************

.. index:: for

This ``for`` statement is used to iterate through the elements of a sequence.

.. code-block :: python

    for element in sequence:
        BLOCK_OF_INSTRUCTIONS

Such a loop will have exactly ``len(sequence)`` revolutions so that the block of instructions will be called exactly as many times as sequence elements. Let's look at an example.

.. activecode:: en_main_0801
    :caption: for loop example

    for el in 'Ala has a cat':
        print ('element: ' + el)

We can also declare a variable and then iterate over it:

.. activecode:: en_main_0802
    :caption: another example of a sequence

    seq = (1, 25, 100, 10000)
    for el in seq:
        print('squared root of', el, "=", el ** 0.5)

Of course, in place of the variable ``seq`` we can put any variable with elements: list, tuple, string, or dictionary. In the case of the latter, we will iterate over its keys.

.. activecode:: en_main_0803
    :caption: dictionaries and for loop

    birthday = {'Amanda': 'August 30', 'Barbara': 'February 17', 'Carol': 'August 2'}
    for name in birthday:
        print(name, 'has birthday', birthday[name])


.. index:: range

``range`` function
~~~~~~~~~~~~~~~~~~

``range`` is a handy function that creates iterable objects that we can use directly in a ``for`` loop. It creates a sequence of integers.

.. code-block:: python

    range(start, stop, step)

It is pretty similar to sequence slicing. This function will return a sequence of numbers, starting with the value ``start`` and ending with ``stop - 1`` exactly every ``step``. Let's see an example.

.. activecode:: en_main_0804

    seq = range(1, 11, 2)
    for el in seq:
        print(el)

The above code will display the numbers ``1, 3, 5, 7, 9``. The numbers ``start`` and ``step`` are optional. If we give only one number ``A`` to the function ``range(A)`` we will get all integers from ``0`` to the number ``A - 1``. If we give two numbers, the first one will be interpreted as ``start`` and the second one as ``stop``. We can only define the step by specifying all three numbers. Of course, negative quantities are possible, including a negative value for a ``step``. It gives us the possibility of creating a list of decreasing numbers. We need to remember that the initial value should be greater than the final value.

.. activecode:: en_main_0805
    :caption: for loop iterating through the sequence generated from the range function

    for number in range(200, 100, -25):
        print(number)

.. topic:: Exercise

    We revisit the exercise from the previous lesson. Use the ``range`` function, ``if`` and ``for`` statements to find the sum of all numbers divisible by 5 and 7 in the range 2998 to 4444. You should get the value 152110.

.. index:: iterator

The function 'range' creates a special type of sequence called an **iterator**. We will not go into details now, but as this name can often be found in tutorials or books on the Python language, it is worth remembering that if we use such an iterator in a loop, the result of its operation will be the same as the typical sequence (like lists). Using iterators, we save memory because they do not create a complete list in the computer's memory, but at each iteration, they calculate the next value, remembering only the present value, the step, and the final value.

The ``enumerate`` function
~~~~~~~~~~~~~~~~~~~~~~~~~~

Sometimes, having a sequence, we want to iterate over those elements which are in some place of interest to us. For example, in the second half of a list or positioned at the odd indices. We can successfully use element indices, but the ``for`` loop iterates over elements, not indexes. In order not to use standard solutions for this, i.e. to introduce an additional variable that will play the role of an index, as below

.. code-block:: python

  idx = 0
  for element in sequence:
      if idx > len(sequence)/2:
          # do something with the elements
          # from the second half of the sequence

we can use the ``enumerate(sequence)`` function which returns a tuple of ``(index, element)`` pairs. To use both quantities simultaneously, we can write

.. activecode:: en_main_0806
    :caption: example enumerate

    sequence = sorted([123, 12, 12, 1, 4, 1, 124, 13, 441])
    sum = 0
    for index, element in enumerate(sequence):
        if index > len (sequence)/2:
            sum += element
    print('Sum of items greater than median:', sum)

.. topic:: Exercise

    ABC
