.. _subroutines:

************
Subroutines
************

From time to time, to make a decision, we decide that blind fate will decide for us. At such moments, we often `throw a coin <https://en.wikipedia.org/wiki/Coin_flipping>`_ and depending on whether it is heads or tails, we choose a specific solution. For example, soccer or basketball games start with a coin toss. This method ensures that the selection of a winning side is completely random and does not depend on any previous throws. Sometimes we resolve a dispute with such a throw and hope that the coin will show that we are right. Maybe we win once, maybe three times, but each throw is independent! We can always calculate the chance that (a) always or (b) half of the time we will win. We use the binomial distribution for such calculations, which will tell us the probability of :math:`k` successes in \\(n\\) trials.

.. math::

    \frac{n!}{k!(n-k)!} p^k (1-p)^{n-k}

Value :math:`p=1/2` means we have an equal chance of rolling heads or tails, :math:`p^k` denotes exponentiation, and :math:`n! = 1 \cdot 2 \cdot \dots n` stands for the factorial of \\(n\\). Exponentiation has its operator in Python, and we can write this operation as ``p ** k``. Factorial appears as a function in the ``math`` library, but it is easy to calculate using the given formula and some loop. The following code calculates the factorial of \\(10! = 3628800\\).

.. activecode:: pl_main_0901
    :caption: factorial

    n = 10
    factorial = 1
    for number in range(1, n + 1):
        factorial = factorial * number
    print(factorial)


We already know how to calculate factorial and power. Therefore we can calculate the chance of winning the bet five times out of 10 tosses. We go back to the formula, but we will substitute specific numbers.

.. math::

    \frac{10!}{5!(10 - 5)!} 0.5^5 (1-0.5)^{10-5}

We need to calculate the factorial of 10 and 5 and take :math:`1/2` to the fifth power.

.. activecode:: en_main_0902
    :caption: 5 coin tosses wins in 10 tries
    :enabledownload:


    n = 10
    factorial10 = 1
    for number in range(1, n + 1):
        factorial10 = factorial10 * number

    n = 5
    factorial5 = 1
    for number in range(1, n + 1):
        factorial5 = factorial5 * number

    p5 = (factorial10 / (factorial5 * factorial5)) * (0.5 ** 5) * ((1 - 0.5) ** (10 - 5))
    print ('Chance to win five times in 10 tries is {: .1f}%'. format(p5 * 100))

Did you expect 50%? It's easy to fall into this trap ... Fortunately, this is not a combinatorics class but a programming course. Let's go back to the above code for a moment. The lines that calculate factorial of 10 (1-4) and 5 (6-9) are copies of the same code. The only difference is the value of the variable ``n`` and the names of the result variables (``factorial5`` and ``factorial10``). Probably the easiest way to calculate the probability of 5 wins was if we had the operator ``!``, Which computes the factorial of a given natural number. We will not change the standard Python library, but in a way, we can program any new functionality by building a **subroutine**. We can divide subroutines into procedures and **functions**. In Python, we only deal with the latter.

.. index:: function

Function
===============

By function, we mean a named piece of code that we can reference repeatedly. To build a function, we use the following syntax.

.. code-block:: Python

  def function_name(<argument_list>):
      BODY_FUNCTION
      <return object>

The objects you see in angle brackets (``argument_list`` and ``return object``) are not mandatory and can be omitted when building a function. Usually, however, the function will transform the argument list (**input data**) into that ``object``, which will later return (**output data**) with the **return** statement. For this tutorial, let's agree that we will build functions containing the **return** keyword.

Let's try to rewrite the program that calculates the chance of 5 wins using the factorial function. The input data is the number ``n``, and the output data is the factorial of this number.

.. activecode:: en_main_0903
    :caption: 5 coin tosses wins in 10 tries, this time with the function
    :enabledownload:

    def factorial(n):
        f = 1
        for number in range(1, n + 1):
            f = f * number
        return f

    factorial10 = factorial(10)
    factorial5 = factorial(5)
    p5 = (factorial10 / (factorial5 * factorial5)) * (0.5 ** 5) * ((1-0.5) ** (10-5))
    print ('Chance to win five times in 10 tosses is {: .1f}%'. format(p5 * 100))

It's not an operator, but it's just as simple to use - we need to call a function name (here is ``factorial``), give the required argument list (there is only one - ``n``), and capture the return value with some variable.

.. code-block:: Python

  result = factorial(10)

We can also go a step further, and instead of calculating the chance as above, we can also write a function that would calculate it. We can even name it so that it leaves no doubt. Once programmed, we can use the ``factorial`` function as many times as we like.

.. activecode:: en_main_0904
    :caption: 5 coin tosses won in 10 tries, version with two functions
    :enabledownload:

    def factorial (n):
        f = 1
        for number in range(1, n + 1):
            f = f * number
        return f

    def chance_of_k_wins_in_n_coin_tosses(k, n):
        chance = (factorial(n) / (factorial(k) * factorial(n-k))) * (0.5 ** k) * (0.5 ** (n-k))
        return chance

    print ('Chance to win five times in 10 tosses is {: .1f}%'. format (chance_of_k_wins_in_n_coin_tosses(5, 10) * 100))


.. topic:: Exercise

  Try to program a function that will calculate the chance of \\(k\\) wins in \\(n\\) attempts for any game, not just *fair coin toss* where heads and tails are just as frequent, but for games where it does not have to be the case (e.g., when throwing a counterfeit coin). We need to write a function that considers the different values ​​of the variable :math:`p \in [0, 1]` in the first formula of this lesson, not just 0.5 (like for the fair coin). In that case, the function must have a list of 3 arguments ``k, n, p``. You can use the above *ActiveCode* or program such function in your favorite development environment.
