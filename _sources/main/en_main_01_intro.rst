*******
Preface
*******

In 1977, Ken Olsen, founder and longtime director of Digital Equipment Corporation, stated *"There is no reason anyone would want a computer in their home."*. Interestingly, he himself was the owner of such a computer. If we talk about the computers, it was of course prehistoric times. Today, no one is wondering about the usability of computers. Sometimes they help to heal people, sometimes they are simply used to keep in touch with friends. Personal computers can be found in over 48% of households around the world. It is used on average by 15% of people in the world. Slightly more than 35% of us uses smartphones, also a type of computer. If we add to the fact that we use the Internet on average for over 6 hours a day, it turns out that the prophecy of Mr. Olsen was one of the most missed opinions in history. In total, we stay in two unreal lands for more than half of our lives as we sleep on average nearly 7 hours a day.

Most of us use computer, smartphone or tablet. We all use the browser to browse the internet or dedicated applications to view or edit photos. We use programs to write letters, essays, emails and to contact via social media. Some of us work using more specialized applications, others just need a web browser and email client.

Sometimes we encounter a situation in which the program we are currently using allows us to perform all necessary tasks, sometimes we lack some functionality and we have to solve the task differently. Sometimes you can find a different program that will do, sometimes not. Sometimes the implementation of scheduled tasks will be extremely time-consuming, but possible to be carried out using the familiar computer programs. Then we only need time and patience (and preferably someone who will check if everything is all right).

Learning the programming language is not about mastering one of the programming languages. There are thousands of these - from the most `popular <https://www.tiobe.com/tiobe-index>` _ like Java, C, Python, C ++, C #, PHP, to those extremely esoteric as already historic INTERCAL or minimalistic Brainf*ck.

This is obviously a necessary step, but it is not the main goal.
The most important skill you acquire is the ability to understand the heart of the problem we are facing and to correctly divide it into its first parts, such as we will be able to easily solve and then put these solutions together so as to get the answer to the original question. This is how a typical programmer works. This method even has its name - divide and conquer and is one of the main programming and algorithms paradigms.

Learning of this template can be the greatest added value of this course. You can try to find a way out of different life situations just by solving smaller parts of the big problem you face. Sometimes it is also worth to program your next day in a step by step manner.

.. note::
    You can try the following programs right away in a Jupyter notebook (IPython).
    Please download this file, upload it to your Jupyter server or open it
    at Google Colaboratory.

    file: `notebook <https://nbviewer.org/urls/bitbucket.org/ccinfinite/ccinfmanual/raw/e5c92e60beec5ca1cf18535691e9abbf063da7bf/_sources/main/en_main_01_intro_code.ipynb>`_

How to organize photographs?
============================

Time for some example, some more specific application of this whole programming idea. Many people have a digital photo archive on their computers or on external hard drives. We often keep these photos in catalogs, we name these catalogs with a place we have visited or with the event we attended. Sometimes we add a date to the name, and sometimes we have parent catalogs associated with the date, e.g.

::

    -2019
      ᒻ--Trip to Krakow
      ᒻ--Fireworks show
      ᒻ--John birthday party

    -2018
      ᒻ--Holidays in Bari
      ᒻ--Weekend in Paris

The pictures that are inside, usually have the name given by the camera, that usually look like this

::

  IMG20180821232.JPG  IMG20180821233.JPG  IMG20180821234.JPG  IMG20180821235.JPG
  IMG20180821236.JPG  IMG20180821237.JPG  IMG20180821238.JPG  IMG20180821239.JPG
  IMG20180821240.JPG  IMG20180821241.JPG  IMG20180821242.JPG  IMG20180821243.JPG
  IMG20180821244.JPG  IMG20180821245.JPG  IMG20180821246.JPG  IMG20180821247.JPG

Maybe, just out of passion for order, we would like the photos in the catalogs to also have significant names that immediately identify the content of the photo, for example `2018_Holidays_in_Bari_001.jpg`. The simplest and most burdensome method will simply be to manually rename all these files. Another might be the use of one of the graphics programs that can make such a change in the so-called batch mode. You can also write a simple Python program ...

.. literalinclude:: main_intro_example1.py
   :linenos:
   :caption: Link to :download:`the script <main_intro_example1.py>`.

The only thing you should know is that instead of "2019" we have to enter the name of the catalog, or even the year we are interested in, and in the place "John birthday party" we enter the name of the directory where we store photos whose names we just want to change. Of course - we also need to know how to run such a program, but we do not have to worry about the name change itself or the fact that we will make a mistake when numbering photos. Once we master the ability to run Python programs that we wrote ourselves we will use the same commands every time.

It looks a bit complicated, but we will learn everything soon. What's important is that we only need 12 lines of code (although there are actually 10 of them) for the computer to do all the work for us. On top of that, this program can be changed quite simply so that it searches the main folder in which we keep the photos and automatically changes the names of all files in a similar way - giving at the beginning of the name of the photo a year, then the name of the event and finally its own numbering.


OK, time for a more academic problem ...

How often does a word appear in written text?
====================================================

Surely everyone who reads books once wondered which word occurs
most often. Or, is there any rule describing the relation
with the frequency of the word and the rank of the word...
Ok, probably no one is really thinking of such things,
rather everyone is focusing on the content of the book ;)
Nevertheless, this is quite an interesting problem (at least for us).

The whole idea is to count how many times a given word occurs in
considered text. If we consider a simple text, e.g. the song "Hello, Goodbye" by The Beatles, then we can simply count how many times a given word appears in it.

    You say, „Yes”, I say, „No”

    You say, „Stop” but I say, „Go, go, go”

    Oh no

    You say, „Goodbye”, and I say, „Hello, hello, hello”

    I don’t know why you say, „Goodbye”, I say, „Hello, hello, hello”

    I don’t know why you say, „Goodbye”, I say, „Hello”

Let's not take into account the case of letters, these are still the same words. We recommend everyone count the number of times a given word appears (there are 15 of them) and then arrange the words from the most common to the most rare. You should get similar table

========== ==========
say        10
i          7
hello      7
you        5
go         3
goodbye    3
no         2
don’t      2
know       2
why        2
yes        1
stop       1
but        1
oh         1
and        1
========== ==========

Is that correct? If the text is simple, then the task is not difficult. Let's try with a slightly more complicated text.

    Having conceived the idea he proceeded to carry it out with
    considerable finesse. An ordinary schemer would have been content
    to work with a savage hound. The use of artificial means to make
    the creature diabolical was a flash of genius upon his part. The
    dog he bought in London from Ross and Mangles, the dealers in
    Fulham Road. It was the strongest and most savage in their
    possession. He brought it down by the North Devon line and walked
    a great distance over the moor so as to get it home without
    exciting any remarks. He had already on his insect hunts learned
    to penetrate the Grimpen Mire, and so had found a safe
    hiding-place for the creature. Here he kennelled it and waited
    his chance.

    But it was some time coming. The old gentleman could not be
    decoyed outside of his grounds at night. Several times Stapleton
    lurked about with his hound, but without avail. It was during
    these fruitless quests that he, or rather his ally, was seen by
    peasants, and that the legend of the demon dog received a new
    confirmation. He had hoped that his wife might lure Sir Charles
    to his ruin, but here she proved unexpectedly independent. She
    would not endeavour to entangle the old gentleman in a
    sentimental attachment which might deliver him over to his enemy.
    Threats and even, I am sorry to say, blows refused to move her.
    She would have nothing to do with it, and for a time Stapleton
    was at a deadlock.

Not so easy. First of all - we are dealing with much more words. Secondly, they usually do not repeat at all. We will not be presenting the entire table here, as there are as many as 154 rows, but the most common words look like this

========== ==========
the        14
to         11
his        9
it         8
a          8
and        8
he         7
was        6
with       4
of         4
in         4
would      3
========== ==========

The computer is the perfect tool to perform the same tasks over and over again.
Computer scientists say that to do the same thing many times, you have to tell the computer to use *loops*.
You have already seen an example of its use in the previous problem.

In the following program, the idea of using a loop occurs three times. In the first, we check letter by letter whether the character of the original text appears in the English alphabet. The second time we count the words one by one. The third `for` loop is used to show results - from the most common to the rarest word.

The program itself is interactive, you can start it by clicking the 'Run' button, you can also change, break and repair it.

.. activecode:: Zipf_law
   :coach:
   :caption: An interactive program that counts the number of times a given word appears in the text.

   txt = """
   You say, „Yes”, I say, „No”
   You say, „Stop” but I say, „Go, go, go”
   Oh no
   You say, „Goodbye”, and I say, „Hello, hello, hello”
   I don’t know why you say, „Goodbye”, I say, „Hello, hello, hello”
   I don’t know why you say, „Goodbye”, I say, „Hello”
   """

   fixed_txt = ""
   for letter in txt.replace("\n", " "):
       l = letter.lower()
       if l in "abcdefghijklmnopqrstuvwxyz ’":
           fixed_txt += l

   zipf = {}
   for word in fixed_txt.split():
       if word in zipf:
           zipf[word] += 1
       else:
           zipf[word] = 1

   for z in sorted(zipf.items(), key=lambda x: x[1], reverse=True):
       print("{0[0]}\t{0[1]}".format(z))

Take a look at lines 11, 17 and 23. You can see keywords ``for``. It means that we have just programmed three loops. Loop is a mechanism that allows computer to perform some tasks over and over again.

Now, try to replace the text with another one.


Well, but how would it work for the whole book? The above excerpt from prose comes from the detective novel by Sir Arthur Conan Doyle, titled The Hound of the Baskervilles. In the translation that you will find on the Project Gutenberg web page `gutenberg.org <https://www.gutenberg.org/ebooks/2852>` _ the whole text has over 62,000 words and sets a rather impossible task for us. That is, you can of course count the given words using a systematic method, but, this type of sequential "find and add" tasks should be done by the machine.

So how do you handle the whole book?

.. literalinclude:: main_intro_example2.py
   :linenos:
   :emphasize-lines: 4,10,20
   :caption: Link to :download:`this script <main_intro_example2.py>`.

Copy this code to the Jupyter notebook, or download and run the program in the console. It should calculate the cardinality and show you the first 10 most common words in the book.


Now test your knowledge.

.. mchoice:: question1_intro
    :multiple_answers:
    :correct: b,c
    :answer_a: All words are equally probable.
    :answer_b: The word in the position (n) appears (1/n) times as often as the most frequent one.
    :answer_c: Probability of an observation is inversely proportional to its rank.
    :answer_d: We should read more books.
    :feedback_a: Not really. Think how popular is the word "the", and how unpopular is the word "Brexit".
    :feedback_b: True. This is the most common statement for the Zipf's law.
    :feedback_c: Correct. That would be the most general statement for the Zipf's law.
    :feedback_d: Although we definitely should read a lot, Zipf never stated that.

    What does Zipf's law mean?


.. fillintheblank:: intro_fill_01

    What is the Python command for loop?

    - :for|while: Exactly!
      :.*: Not really... please refer to the text above.


.. index:: comment

Comments
=========

The ``#`` character precedes python comments.
All characters following ``#`` and extending to the end of the line are
ignored [#comment]_.

.. code-block:: python

   >>> # the first line of the comment
   >>> print("Hello World!") # second line of the comment

Documentation of functions or classes made up of text enclosed in triple quotation marks or apostrophes can be considered a kind of comment. It is not a *per se* comment, but it may be. You can read more about the documentation later in this manual.

.. code-block:: python

   """
   this text is preceded by,
   and ended with three quotation marks,
   is, therefore, an ordinary character literal,
   which is often used for
   creating program documentation
   """

A programming language
======================

A computer program created using a programming language is the most common method of making a computer do the work we have planned. If this job is not about replying to emails but rather about doing some recurring activities that are less standard, then writing your own program will probably be the most straightforward method. It may sound strange at the very beginning of learning programming, but we hope you will change your mind by the end of this course. We all can understand programming language as communicating method with a computer that is understandable to a human. We can compare it to a natural language that enables the communication between people. There is one difference, however. In the case of programming languages, there is no room for understatement or interpretation. We must state all messages very precisely. The computer is a machine and cannot (yet) work out the actual ambiguous expression.




.. rubric:: Footnotes

.. [#comment] can sometimes lead to problems between the coding standard of the text used in the script, and the one declared in the script header. It is safest to use the UTF-8 system.
