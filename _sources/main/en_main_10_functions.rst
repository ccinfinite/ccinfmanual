.. _concept_of_function:

*************************
The concept of a function
*************************

This part of the tutorial will be a slightly more formal introduction to functions. We will provide a complete definition and discuss formal parameters, arguments assigned by name, and the return statement. You can take it as a development of what we described in the previous chapter and look up here whenever something related to functions becomes unclear.

.. index:: function

As you may have read before, a named piece of code that we can reference multiple times will be called a function. In Python, we build functions with the keyword ``def``.

.. code-block:: Python

  def function_name (<argument_list>):
      BODY_FUNCTION
      <return object>

The characteristic feature of function is the result value. In Python, this value is declared by the ``return`` statement inside the function body.
Using the functions is intuitive. We can assume that where it is necessary to repeat the code or execute the same procedure, we should transform it into a function. The program becomes more readable and functional. The idea of ​​a function in programming is analogous to a function in a mathematical sense. However, unlike mathematical functions, programming functions do not need to have an argument (parameter) to be correctly defined. For example:

.. activecode:: en_main_1001
   :caption: Function with no parameter.

   def hey_function():
       return "Hey! How are you?"

   hey = hey_function()
   print(hey)

The ``return`` statement
========================

.. index:: return

In the examples above, the functions interact with us over the ``return`` statement. This instruction passes the given object and causes the function to terminate the calculation. If we want to pass more than one variable, we can use a tuple, list, or dictionary. To call a function, write its name followed by a tuple containing the arguments to the function. The order and the number of parameters passed to the function must comply with its definition:

.. activecode:: en_main_1002
   :caption: Function that calculates the value of the square of the sum.

   def squared_sum(x, y):
       return (x + y) ** 2

   print(squared_sum(2, 1))

Note that you can use the ``return`` statement multiple times in your function code, but the function will stop executing and exiting when any of them is reached. For example

.. activecode:: en_main_1003
   :caption: A function that returns the greater of the two given numbers.

   def maximum (x, y):
       """A function that returns the greater of two numbers."""
       if x > y:
           return x
       else:
           return y

   print(maximum(10, -10))

.. index:: docstring

The function's execution stops either after the ``if`` statement or after the ``else`` statement. It is a good practice to describe what the function is for in the form of a string ``"""..."""``. A description, placed immediately after the first line that defines the function name, is called **docstring**. In this course, we tend to discuss the programs in detail. To keep things simple, we will not always respect this practice in the following examples. You should, however.

.. topic:: Exercise

    Write a function that checks whether the given number is even or odd.

Default parameter values
============================

.. index:: default parameters

When defining a function, we can assign default values ​​to the parameters. Default values will be used when no other arguments ​​are given for these parameters at the function call.

.. topic:: **Difficult Physics Problem**

    You threw the ball up. Calculate how high the ball will rise after \\(t\\) seconds if the initial velocity value was \\(v_0\\) m∕s. Neglect the resistance force and everything else except gravity. Use the formula

    .. math::

        h(t) = h_0 + v_0 t - \frac{1}{2} g t^2

In the formula above, \\(g\\) is the gravitational acceleration, which for the Earth is approximately \\(g = 9.80665 m/s^2\\). We allow low precision calculations and pretend \\(g\\) is a constant value (although it is not ...). As the initial height and velocity and the time we measure the height will be different depending on the experiment, we should leave them as variables without the default values. For the gravitational acceleration \\(g\\) we can assume that, as a rule, such a throw will take place on the ground (\\(h_0=0\\). In this case, we can give the default values for the Earth's gravitational acceleration ``g`` and initial height \\(h_0\\) in the definition of function ``height``.

.. activecode:: en_main_1004
   :caption: Function with predefined parameter values.

   def height(t, v0, h0=0, g=9.81):
       """Height function:
          t - time
          v0 - initial speed
	  h0 - inital height, default value is set to zero
          g - gravitational acceleration, default for Earth
        """
       return h0 + v0 * t - g * t ** 2 / 2

   print (height(0.3, 3))  # on Earth
   print (height(t=0.3, v0=3, g=1.622))  # on the Moon

.. topic:: Exercise

    Write a function that converts the distance given in kilometers to miles. By default, convert to nautical miles.


An undefined number of function parameters
==========================================

Python allows you to construct a function that does not have the specified number of parameters defined. To do this, precede the name of the function parameter list with the symbol ``*``. We can call such a function with any number of arguments. The function presented below sums up all values passed as arguments.

.. activecode:: en_main_1005
   :caption: A function with an undefined number of parameters.

   def addition(*arg):
       s = 0
       for a in arg:
           s += a
       return s

   print(addition(10))
   print(addition(10, 20, 30, 40))

How will this design work in conjunction with other parameters? Let's see.

.. activecode:: en_main_1006
   :caption: An example of a function where the initial value of the parameters is unknown.

   def addition(arg1, * arg):
       s = arg1
       for a in arg:
           s = s + a
       return s

   print(addition(100, 2))
   print(addition(100, 1, 2, 3, 4))

Nesting functions
=====================

.. index:: nested functions

We can include function definitions inside the body of other functions. The program below calculates the value of the cosine of the angle between vectors on the plane:

.. activecode:: en_main_1007
   :caption: Containing a function within itself.

   def cosine(a, b):

       def length(x):
           return (x[0]**2 + x[1]**2)**0.5

       def product(x, y):
           return x[0]*y[0] + x[1]*y[1]

       m = length(a) * length(b)
       if m > 0:
           return product(a, b) / m

   print(cosine([1, 0], [0, 1]))


.. topic:: Exercise

    Write a function that for the given ``a``, ``b`` and ``c`` finds solutions to the quadratic trinomial.

    .. math::

        a x^2 + bx + c = 0

    The program should inform about the number of solutions and return them.
