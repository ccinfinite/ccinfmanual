.. _modules:

.. index:: module

**********
Modules
**********

.. index:: decomposition, abstraction

When creating a computer program and solving a problem with its help, we usually build several functions to solve specific parts of the problem. In :ref:`a lesson on functions <subroutines>` we wrote two functions - one calculated the factorial and the other the probability of a certain number of wins in a few coin tosses. The first one (``factorial``) was an auxiliary function that allowed for the reduction of the code, improved its readability, and limited the possibility of making a mistake. In other words, it led to a **decomposition** of the main problem into sub-problems (or rather their solutions). Moreover, it made the future use of the factorial function independent of its implementation. We could improve this code in some way (e.g., start ``range`` with number 2 instead of 1). This would not affect the solution to the coin toss problem, which we built with the ``factorial`` function. Such independence of the implementation from the usage is called **abstraction**. Decomposition and abstraction are the two main reasons to use functions.

.. index:: import

There are more problems of combinatorics that require the computation of factorials. All in all, if we were to work on combinatorics problems, it would be nice to have this function at hand and use it without having to define the factorial function each time. One option is to use such a function programmed in the ``math`` module. To use it, we need to import it.

.. code-block:: python

  from math import factorial

From now on, we can use it just like we used the function we wrote. So we can calculate how many different ways we can form the letters "abc". We will use the formula for the permutation of a 3-element set - the answer is :math:`3!` (look at any maths textbook)

.. activecode:: en_main_1101
   :caption: Permutations and factorial

   from math import factorial
   word = 'abc'
   ans = factorial(len(word))
   print('{}! = {}'.format(len(word), ans))

Technically speaking, a math module is just a file that contains Python definitions and instructions. The file name is the name of the module appended with the ``.py`` extension.

.. index:: import, from

Use of modules
=====================

To use the functions in the modules, we need to import them into the namespace. We have two options. You can find the first one above.

.. code-block:: python

  from module import object
  # e.g
  from math import factorial

We can now refer to an ``object`` (a function) as if defined by ourselves in the program we are working on, just like the example above. If we want, we can change the name of the imported object to whatever we like using the construction ``from module import object as nice_name``.

.. activecode:: en_main_1102
   :caption: Importing a function from a module

   from math import factorial as fact
   word = 'abc'
   ans = fact(len(word))
   print('{}! = {}'.format(len(word), ans))


If we need several functions from one module, we can import them as a list.

.. code-block:: python

  from module import object1, object2
  # e.g
  from math import factorial, pow

It will enable the use of both functions in the program. In the example below, we imported the functions ``factorial`` as ``fact``, and the function ``pow`` with unchanged name.

.. activecode:: en_main_1103
   :caption: Import two functions from the module

   from math import factorial as fact, pow
   k, n = 5, 10
   print('The probability {} of wins in {} coin tosses equals '.format(k, n), end = '')
   print('{:.1f}%'.format(100 * fact(n) * pow(0.5, 10) / fact(k)**2))


The second way is to import the entire module and refer to the functions (and other objects) it contains with a *dot* reference. Here, too, we can import a module using a name that suits us best.

.. code-block:: python

    import module
    import module as other_name

We will calculate once again how many ways we can arrange the letters "abc", but we will call the function through the module and the dot reference.

.. activecode:: en_main_1104
   :caption: dot reference to a function

   import math
   word = 'abc'
   ans = math.factorial(len(word))
   print('{}! = {}'.format(len(word), ans))


Our module
=================

If we wanted to build our module containing, for example, various functions useful in combinatorics, it is enough to create a file with a meaningful name and include these functions in this file. In our case, we will call the file, and therefore the module ``combinatorics.py``. We will add all the functions that we programmed in the previous lessons.

.. literalinclude:: combinatorics.py
    :linenos:
    :emphasize-lines: 1, 17
    :caption: Download :download:`combinatorics module <combinatorics.py>`.

To use the newly created library, you need to import it using any of the methods described above.

.. literalinclude:: en_main_ch11e01.py
    :linenos:
    :emphasize-lines: 1
    :caption: Download :download:`this script <en_main_ch11e01.py>`.
